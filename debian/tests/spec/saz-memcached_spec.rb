require 'spec_helper'

describe "memcached is not installed" do
  describe package('memcached') do
    it { should_not be_installed }
  end
  describe service('memcached') do
    it { should_not be_running }
  end
end

describe "main memcached class applies successfully" do
  describe command("puppet apply --logdest /var/log/puppet-apply.log --test -e 'include memcached'") do
    its(:exit_status) { should eq 2 }
  end
end

describe "memcached is installed" do
  describe package('memcached') do
    it { should be_installed }
  end
end

describe "memcached service is started" do
  # allow time for the service to start
  before(:all) do
    sleep 5
  end
  describe service('memcached') do
    it { should be_enabled }
    it { should be_running }
  end
end

describe "memcping can reach the server" do
  describe command('memcping') do
    its(:exit_status) { should eq 0 }
  end
end

describe "memccp can insert data" do
  describe command('memccp spec/fixtures/test') do
    its(:exit_status) { should eq 0 }
  end
end

describe "memccat can retrieve data" do
  describe command('memccat test') do
    its(:exit_status) { should eq 0 }
    its(:stdout) { should include "d9ae4ba3-3ad3-4648-9af0-44ffa4a352d2" }
  end
end

describe "memcached::instance applies successfully" do
  describe command("puppet apply --logdest /var/log/puppet-apply.log --test -e 'memcached::instance{\"11222\":}'") do
    its(:exit_status) { should eq 2 }
  end
end

describe "memcached@11222 service is started" do
  # allow time for the service to start
  before(:all) do
    sleep 5
  end
  describe service('memcached@11222') do
    it { should be_enabled }
    it { should be_running }
  end
end
